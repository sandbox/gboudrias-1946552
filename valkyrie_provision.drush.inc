<?php
/**
 * @file
 * Drush commands for DevShop Provision
 */

/**
 * Implements hook_drush_init()
 * - Ensures that provision is loaded before valkyrie_provision
function valkyrie_provision_drush_init(){
  $list = drush_commandfile_list();
  $provision_drush_inc = $list['provision'];
  include_once($provision_drush_inc);
  include_once('valkyrie_provision.context.project.inc');
}
 */

/**
 * Implementation of hook_drush_command().
 * Provides provision commands for all valkyrie tasks.
 */
function valkyrie_provision_drush_command() {
  $items['provision-valkyrie-init'] = array(
    'description' => 'Export the site\'s Features and commit the result.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'options' => array(
    ),
    'arguments' => array(
      'environment' => 'The name of the environment to initialize.',
    ),
    /*
    'examples' => array(
      'drush @project_mysite provision-valkyrie-commit dev --message="changed some settings"' => 'Recreates and Commits all features from the dev environment of @project_mysite with an additional commit message.', 
    ),
    */
    'aliases' => array('pvi'),
  );
  return $items;
}

/**
 * Function for checking if this is a project and we have a repo.
 *
 * Used in pre drush command hooks
 */
function valkyrie_provision_pre_flight($platform_name = NULL){

  if (d()->type != 'project'){
    return drush_set_error(DEVSHOP_FRAMEWORK_ERROR, 'All provision-valkyrie-* commands must be run on a project alias.');
  }
}
