<?php

/**
 * Pre provision-valkyrie-init hook
 *
 * Implements drush_hook_pre_COMMAND() hook.
 */
function drush_valkyrie_provision_pre_provision_valkyrie_init(){
  valkyrie_provision_pre_flight();
}


/**
 * Implements the provision-valkyrie-init command.
 */
function drush_valkyrie_provision_provision_valkyrie_init($platform_name = NULL) {
  drush_log(dt('[VALKYRIE] Test command'), 'ok');
}
